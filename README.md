# Alma Letter Config

Just tracking the various Alma xml/xsl changes. U of G not using All or these, only adjusting a few but we'll be able to track changes.

Some testing xml found here: https://developers.exlibrisgroup.com/blog/Alma-letters-XML-samples-for-working-on-XSL-customization/

## Letters UofG currently has turned on

1. Analytics Letter 
2. Borrowing Activity Letter
3. Courtesy Letter
4. Document Delivery Notification Letter
5. Ful Digitization Notification Item Letter Email
6. Ful Fines Fees Notification Letter
7. Ful Lost Loan Letter
8. Ful Overdue And Lost Loan Letter
9. Ful Overdue And Lost Loan Notification Letter
10. Ful Pickup Print Slip Report Letter
11. Ful Transit Slip Letter
12. Interested In Letter
13. Loan Status Notice
14. On Hold Shelf Letter
15. On Hold Shelf Reminder Letter
16. Overdue Notice Letter
17. Query To Patron Letter
18. Reset Password Letter
19. Resource Sharing Receive Slip Letter
20. Resource Sharing Return Slip Letter
21. Saved Searches Letter
22. Shortened Due Date Letter
23. System Job Letter

## To Discover Changes
If you don't want to use the entire file, you can see the specific changes:

1. Navigate to the Letters/*letter name*
2. Click on History in the top right of nav bar -> this will give you the various commits / recorded changes
3. Select the commits, starting at the oldest (bottom) and work your way through them to view the individual changes
